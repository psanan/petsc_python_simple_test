import PetscBinaryIO as pio

io = pio.PetscBinaryIO();
fh = open('out.petscbin')
objecttype = io.readObjectType(fh)
if objecttype == 'Vec':
  v = io.readVec(fh)
  print(v)
