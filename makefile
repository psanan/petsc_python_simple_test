EXNAME=main

all: ${EXNAME}

.PHONY: all

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

${EXNAME}: ${EXNAME}.o
	-${CLINKER} -o ${EXNAME} ${EXNAME}.o  ${PETSC_KSP_LIB}
	${RM} ${EXNAME}.o

clean :: 
	${RM} ${EXNAME} out.petscbin*
