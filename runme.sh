#/usr/bin/env bash
# Set PETSC_DIR and PETSC_ARCH in your environment first
# You should see 100 7's printed out!

PYTHONPATH=$PYTHONPATH:$PETSC_DIR/lib/petsc/bin
make clean
make
./main
python test.py

