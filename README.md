# Simple test of reading a PETSc binary file with Python

Make sure you have a working build of PETSc, and that `PETSC_ARCH` and` PETSC_DIR`
are set in your environment. You also need a working `python` and `bash`.

Then try

    ./runme.sh

This should build a simple PETSc program, run it to generate a PETSc binary file, and then open the file and print out the result with Python.

For more on the Python functions, see `$PETSC_DIR/bin/PetscBinaryIO.py`.

